import typescript from 'rollup-plugin-typescript2'
import commonjs from 'rollup-plugin-commonjs'
import external from 'rollup-plugin-peer-deps-external'
import resolve from 'rollup-plugin-node-resolve'

import pkg from './package.json'

export default {
  input: 'src/index.ts',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true
    },
    {
      file: pkg.module,
      format: 'es',
      exports: 'named',
      sourcemap: true
    }
  ],
  plugins: [
    external(),
    resolve({ preferBuiltins: true, browser: true }),
    typescript({
      rollupCommonJSResolveHack: true,
      exclude: '__tests__/**',
      clean: true
    }),
    commonjs({
      include: ['node_modules/**']
    })
  ],
  // Hide build warnings of `node_modules/store/plugins/lib/json2.js`
  // https://rollupjs.org/guide/en/#avoiding-eval
  // https://rollupjs.org/guide/en/#onwarn
  onwarn: (warning) => {
    if (warning.code === 'EVAL' && warning.id.includes('node_modules/store/plugins/lib/json2.js')) {
      return
    }
    // Use default for everything else
    console.warn(warning)
  }
}
